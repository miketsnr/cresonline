import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CalllogComponent } from './calllog/calllog.component';
import { AdminComponent } from './admin/admin.component';
const routes: Routes = [
  { path: '', component: CalllogComponent },
  { path: 'texts', component: AdminComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
