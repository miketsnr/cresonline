import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../utilities/api-service.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminModalContentComponent } from '../admin-modal-content/admin-modal-content.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  lHList = [];
  constructor(private apiService: ApiServiceService, private modalService: NgbModal) { }

  ngOnInit() {
    this.apiService.getHelpList();
    this.apiService.hList.subscribe(data => {
      this.lHList = data;
    });
  }
  openModal(itemin) {
    const modalRef = this.modalService.open(AdminModalContentComponent);
    modalRef.componentInstance.itemin = itemin;
    modalRef.componentInstance.action = 'edit';
  }
}
