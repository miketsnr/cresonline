import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiServiceService } from '../utilities/api-service.service';
import { SearchpipePipe } from '../utilities/searchpipe.pipe';
import { ModalService } from '../_modal';
import { SRVItem } from '../_models';

@Component({
  selector: 'app-problem-id',
  templateUrl: './problem-id.component.html',
  styleUrls: ['./problem-id.component.css'],
  providers: [SearchpipePipe]
})
export class ProblemIdComponent implements OnInit {
  @Output() selectedservice = new EventEmitter();
  public searchlist: SRVItem[];
  products = [{ id: '', name: '', qty: 0, uom: '', group: ''}] ;
  serviceid = { id: '', name: '', qty: 0, uom: '', group: ''} ;
  deladdresses = [];
  packlist = [];
  productlisting = [];
  packproducts = [{ id: '', name: '', qty: 0, uom: '', group: ''}] ;
  packqty = 0;
  packadr = '';
  packdesc = '';  // allow for additional versions
  packname = '';
  packaddrcode = '';
  packatt = '';
  packtel = '';
  ordersout = [];
  public searchbox: any;
  public comment: string;
  subscriber: Subscription;
  okdone = '';
  constructor(private apiService: ApiServiceService, private modalService: ModalService, private searchpipe: SearchpipePipe) {

  }

  ngOnInit() {
    this.okdone = '';
    this.comment = this.apiService.callcontainer.Service.description;
    this.apiService.callcontainer.Service.nodekey = '';
    this.apiService.callcontainer.Service.responsible = '';
    this.apiService.callcontainer.Service.group = '';
    this.apiService.callcontainer.Service.tag = '';
    this.apiService.covidadrlist.subscribe(data => {
      this.deladdresses = data ;
    });
    this.apiService.productlist.subscribe(data => {
      this.products = data;
      let prodheaders = [] ;
      this.products.forEach(item => {
        if (item.group !== 'SERVICE' ) {
          prodheaders.push(item.group);
        } else {
          this.serviceid = item ;
        }
      });
      this.apiService.lcplist.forEach( pack => {
        this.packname = pack.name ;
        this.packdesc = pack.id ;
        const lclitemar = pack.group.split(',');
        lclitemar.forEach( subitem => {
          let lclproduct = this.products.find(element => {
            return element.id ===  subitem.split(':')[0] ;
          });
          lclproduct = JSON.parse(JSON.stringify(lclproduct));
            lclproduct.group = pack.id ;
            lclproduct.uom = subitem.split(':')[1] ;
            if (this.packproducts[0].id !== ''){
            this.packproducts.push(lclproduct);
            } else {
              this.packproducts[0] = lclproduct ;
            }
          });
       });
     prodheaders =  prodheaders.filter((value, index, self) => {
      return self.indexOf(value) === index;
         });
     prodheaders.forEach(item => {
         const lclobj = {groupname: item , products: [] ,  totqty: 0, visible: ''};
         this.products.forEach( prod => {
           if (prod.group === item) {
             lclobj.products.push(prod);
           }
         });
         this.productlisting.push(lclobj);
     });
    });

    this.subscriber = this.apiService.srvList.subscribe({
      next: (item: SRVItem[]) => {
        this.searchlist = item;
        //     const data = this.searchpipe.transform(this.searchlist, [this.searchbox, 'tag']);
        if (item === null) {
          this.comment = this.apiService.callcontainer.Service.description;
          this.apiService.callcontainer.Service.nodekey = '';
          this.apiService.callcontainer.Service.responsible = '';
          this.apiService.callcontainer.Service.group = '';
          this.apiService.callcontainer.Service.tag = '';
          this.okdone = '';
        } else {
          const data = item;
          if (data.length > 1  && data[0].nodekey !== data[1].nodekey){
          this.okdone = 'Z';
          }
          if (data && Array.isArray(data)) {
            if (data.length === 1) {
              this.apiService.callcontainer.Service.description = this.comment;
              this.apiService.callcontainer.Service.nodekey = data[0].nodekey;
              this.apiService.callcontainer.Service.responsible = data[0].responsible;
              this.apiService.callcontainer.Service.group = data[0].group;
              this.apiService.callcontainer.Service.tag = data[0].tag;
    //          this.selectedservice.emit(data[0]);
              this.okdone = 'Z';
            } else {
              if (data.length === 0) {
    //            this.onNoneofAbove();
              this.okdone = '';
              }
            }
          }
        }
      },
      error: (err: any) => {

      },
      complete: () => {

      },
    });
  }
  // tslint:disable-next-line: use-life-cycle-interface
  ngOnDestroy() {
    this.subscriber.unsubscribe();
  }
  showme(heading) {
   heading.visible = (heading.visible) ? '' : 'X' ;
  }

  requestupdated(newtext) {
    if (this.ordersout.length === 0) {
      this.okdone = '';
    }
  }
  validateNum(numberin) {
    return isNaN(numberin);
  }
  onNoneofAbove() {
    this.apiService.callcontainer.Service.description = this.comment;
    this.apiService.callcontainer.Service.nodekey = '0';
    this.apiService.callcontainer.Service.responsible = 'TBA';
    this.apiService.callcontainer.Service.group = 'TBA';
    this.apiService.callcontainer.Service.tag = 'To Be Confirmed by Call Centre Agent';
    this.selectedservice.emit('NoneofAbove');
  }
  onSelect(service: SRVItem) {
    this.apiService.callcontainer.Service.description = this.comment;
    this.apiService.callcontainer.Service.nodekey = service.nodekey;
    this.apiService.callcontainer.Service.responsible = service.responsible;
    this.apiService.callcontainer.Service.group = service.group;
    this.apiService.callcontainer.Service.tag = service.tag;
    if (service.type === 'SERVICE' && this.ordersout.length === 0 ) {
      this.openModal('custom-COVID');
    } else {
      /*   else if (service.description.indexOf('Churn')) {
        this.openModal('custom-churn-1');
       }
       else {*/
      this.selectedservice.emit(service);
    }
  }
  onQtyChange(product) {
    if ( product.qty.length < 1) {
      product.qty = '0';
    }
  }
  onProject() {
    this.ordersout = this.products.filter(item => {
      return item.qty > 0 ;
    }) ;
    if (this.ordersout.length > 0 ) {
      if (this.comment === undefined) {
        this.comment = '' ;
      }
      this.ordersout.forEach( (lineout, index) => {
        const ind = index + 1 ;
        this.comment +=  'Products Ordered: (' + ind + ') '    + lineout.qty + ' x '  + lineout.id + '  ' + lineout.name  ;
      });
   // this.comment +=  '\r\n' + JSON.stringify(this.ordersout);
    this.apiService.callcontainer.Service.description = this.comment;
    this.apiService.callcontainer.Service.nodekey = this.searchlist[0].nodekey ;
    this.apiService.callcontainer.Service.responsible = this.searchlist[0].responsible ;
    this.apiService.callcontainer.Service.group  = this.searchlist[0].group;
    this.apiService.callcontainer.Service.tag = this.searchlist[0].tag;
    this.selectedservice.emit(this.searchlist[0]);
    }
    this.closeModal('custom-COVID');
  }
  onCAREPACK() {
    this.comment = 'SBV Ordered: ' + this.packdesc + '- (' + this.packqty  + ' Packs ) Split as follows: ';
    this.packproducts.forEach(itemin => {
     if (itemin.group === this.packdesc ) {
       const lnqty = +itemin.uom * this.packqty ;
      this.comment +=  '\r\n  (' + lnqty + 'x):' + itemin.id + ': ' + itemin.name ;
     }
    });
    this.packadr = this.deladdresses.find(item => {
      return item.addrcode === this.packaddrcode;
    }).FullAddress ;
    this.comment +=  '\r\nDeliver at: AddrCode:(' + this.packaddrcode + ') '    + this.packadr ;
    if ( this.packatt || this.packtel) {
      this.comment +=  '\r\nAttention: ' + this.packatt + '\r\nTelephone: '    + this.packtel ;
    }
    this.packlist = [{group: 'COVID'}] ;
    this.apiService.callcontainer.Location.addrcode = this.packaddrcode ;
    this.apiService.callcontainer.Service.description = this.comment;
    this.apiService.callcontainer.Service.nodekey = this.packdesc ;
    this.apiService.callcontainer.Service.responsible = ' ' ;
    this.apiService.callcontainer.Service.group  = 'COVID';
    this.apiService.callcontainer.Service.tag = this.packname;
    this.selectedservice.emit(this.packlist[0]);
    this.closeModal('custom-CAREPACK');
  }
  onRegion(id){
    this.packaddrcode = id ;
    this.packatt = this.deladdresses.find(item => {
      return item.addrcode === id;
    }).Attention ;
    this.packtel = this.deladdresses.find(item => {
      return item.addrcode === id;
    }).Telephone;

    this.packatt = (this.packatt === 'Unknown') ? '' : this.packatt ;
    this.packtel = (this.packtel === 'Unknown') ? '' : this.packtel ;
  }
  onChurn() {
    this.selectedservice.emit(this.apiService.callcontainer.Service);
  }
  openlist() {
    this.ordersout = this.products.filter(item => {
      return item.qty > 0 ;
    }) ;
    if (this.ordersout.length > 0) {
   //  this.onSelect(this.apiService.callcontainer.Service);
     this.selectedservice.emit(this.apiService.callcontainer.Service);
     return ;
    }
    this.okdone = 'Y';
    this.comment = this.comment.replace(/"/g, '');
    this.comment = this.comment.replace(/,/g, ' ');
    this.comment = this.comment.replace(/'/g, '');
    this.apiService.getSrvList(this.comment, 'ABSA-00001-01');

  }

  openModal(id: string) {
    switch (id) {
      case 'custom-CAREPACK':
  //   this.searchlist = this.packproducts ;
    // this.apiService.getSrvList('COVID', 'ABSA-00001-01');
      break;
    }
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

}
