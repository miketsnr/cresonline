import { Component, OnInit } from '@angular/core';
import { ThrowStmt } from '@angular/compiler';
import { User, BuLocation, CallModel, SRVItem, CallLog } from './_models';
import { ApiServiceService } from './utilities/api-service.service';
import { SearchpipePipe } from './utilities/searchpipe.pipe';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { QstnmodalContentComponent } from './qstnmodal-content/qstnmodal-content.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'call-logger';
  public focusOn = 0;
  selectedLine = {};
  callcontainer = new CallModel();
  photoToUpload: File = null;
  myImageSrc: any;
  lastNotification: string;
  public titles: any[];

  constructor(public apiService: ApiServiceService, private modalService: NgbModal) {
    // this.apiService.getBuList('');
  }
  ngOnInit() {
    if (localStorage.getItem('calllogged')) {
      this.focusOn = 1;
    }
    this.apiService.photoUploaded.subscribe(data => {
      if (data && this.apiService.callcontainer.Service.description.length > 5) {
        this.focusOn = 6;

      }
    });
    this.apiService.tList.subscribe(data => {
      this.titles = data;
    });
    this.apiService.focusObs.subscribe(data => {
      this.focusOn = data;
    });
    this.apiService.getTitleList();
  }
  selectedBU(item) {
    this.focusOn = 3;
    this.callcontainer = JSON.parse(JSON.stringify(this.apiService.callcontainer));
  }

  handleFileInput(files: FileList) {
    this.photoToUpload = files.item(0);
    const reader = new FileReader();
    const preview = document.querySelector('#myImageSrc');
    reader.onloadend = function () {
      preview['src'] = reader.result;
    };

    if (this.photoToUpload) {
      reader.readAsDataURL(this.photoToUpload); //reads the data as a URL
    } else {
      this.myImageSrc = '';
    }
  }
  // To be called after Notif Created
  uploadPhotoToNotif(notif: string) {
    this.apiService.postPhotoFile(this.photoToUpload, notif).subscribe(data => {
      // do something, if upload success
      console.log(data);
    }, error => {
      console.log(error);
    });
  }
  useridentified(user) {
    this.focusOn++;

    this.callcontainer = JSON.parse(JSON.stringify(this.apiService.callcontainer));
  }

  serviceRequested(service: any) {
    this.focusOn++;
    this.callcontainer = JSON.parse(JSON.stringify(this.apiService.callcontainer));
  }
  questionAnswered(priority: string) {
    this.focusOn++;
    this.callcontainer.RiskAnalysis.priority = priority;
    this.callcontainer = JSON.parse(JSON.stringify(this.apiService.callcontainer));
  }
  onSignup(_f) {

  }
  openModal(itemof) {
    let itemin = { QUESTION: 'contact', LINE1: '', LINE2: '' };
    this.titles.find(data => {
      if (data.KEY == itemof) {
        itemin = data; return true;
      }
    });

    const modalRef = this.modalService.open(QstnmodalContentComponent);
    modalRef.componentInstance.itemin = itemin;
  }
  onSubmit(call: CallModel) {
    this.apiService.createCall(call).subscribe(
      data => {
        console.log(data); // now load photo
        let s1 = data.d.chObjid.split(',')[0];
        const s2 = s1.split(':')[1];
        s1 = s2.replace(/\\"/g, '');
        this.lastNotification = s1.replace(/^0+/, '');
        this.focusOn = 6;
        if (this.photoToUpload) {
          this.uploadPhotoToNotif(s1);
        }
      },
      e => {
        console.log(e);
      }
    );
  }
  goforit() {
    this.focusOn = 1;
    localStorage.setItem('calllogged', 'X');
  }
  greaterThan(subj: any, num: any) {
    return subj > num;
  }
  gTorEq(subj: any, num: any) {
    return subj > num || subj === num;
  }
  lessThan(subj: any, num: any) {
    return subj < num;
  }
  lTorEq(subj: any, num: any) {
    return subj < num || subj === num;
  }
  change(what: string) {
    switch (what) {
      case 'username': {
        this.focusOn = 1;
        break;
      }
      case 'location': {
        this.focusOn = 2;
        break;
      }
      case 'service': {
        this.focusOn = 3;
        this.apiService.currentSRVList.next(null);
        break;
      }
      case 'risk': {
        this.focusOn = 4;
        break;
      }
    }
  }

}
