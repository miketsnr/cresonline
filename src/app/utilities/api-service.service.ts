import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of, BehaviorSubject, Subject, Observable, Subscription } from 'rxjs';
import { SRVItem, BuLocation, CallLog, CallModel, User } from '../_models';
import { environment } from '../../environments/environment';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
  public currentSRVList: BehaviorSubject<any[]>;
  public srvList: Observable<any[]>;
  /*     */
  public currentBUList: BehaviorSubject<any[]>;
  public currentBU: BehaviorSubject<BuLocation>;
  public currentLocation: Observable<BuLocation>;
  public buList: Observable<any[]>;
  public lcplist = [];
  /*  */
   public currentProdList = new BehaviorSubject([]);
   public productlist = this.currentProdList.asObservable();
  /*  */
  public currentBUFloorList: BehaviorSubject<any[]>;
  public floorlist: Observable<any[]>;
  /*    */
  public currentQList: BehaviorSubject<any[]>;
  public qList: Observable<any[]>;
  /*   */
  currentAdrList = new BehaviorSubject<any[]>(null);
  covidadrlist = this.currentAdrList.asObservable();
  /*  */
  public currentTitleList: BehaviorSubject<any[]>;
  public tList: Observable<any[]>;
  /*   */
  public currentHelpTextList: BehaviorSubject<any[]>;
  public hList: Observable<any[]>;
  /*   */
  public callcontainer: CallModel;
  public calllogged: CallLog;
  /*    */
  public fileContent: any;
  public photoLoaded: BehaviorSubject<any>;
  public photoUploaded: Observable<any>;
  /*   */
  public homeurl = '';
  public currentUser: BehaviorSubject<any>;
  public loggingUser: Observable<any>;
  private tempuser: User;
  public currentFocus: BehaviorSubject<any>;
  public focusObs: Observable<any>;

  constructor(private http: HttpClient, private auths: AuthService) {
    this.homeurl = window. location. href ;
    this.init();
  }
  init() {
    this.currentSRVList = new BehaviorSubject<SRVItem[]>(null);
    this.srvList = this.currentSRVList.asObservable();
    this.currentBUList = new BehaviorSubject<BuLocation[]>(null);
    this.currentBUFloorList = new BehaviorSubject<any[]>(null);
    this.floorlist = this.currentBUFloorList.asObservable();
    this.buList = this.currentBUList.asObservable();
    this.currentQList = new BehaviorSubject<BuLocation[]>(null);
    this.qList = this.currentQList.asObservable();
    this.currentTitleList = new BehaviorSubject<[{}]>(null);
    this.tList = this.currentTitleList.asObservable();
    this.currentTitleList.next(this.getTitles());
    this.currentHelpTextList = new BehaviorSubject<[{}]>(null);
    this.hList = this.currentHelpTextList.asObservable();
    this.callcontainer = new CallModel();
    this.calllogged = new CallLog();
    this.currentBU = new BehaviorSubject<BuLocation>(null);
    this.currentLocation = this.currentBU.asObservable();
    this.currentUser = new BehaviorSubject<User>(null);
    this.loggingUser = this.currentUser.asObservable();
    this.currentFocus = new BehaviorSubject<string>('0');
    this.focusObs = this.currentFocus.asObservable();
    this.getCOVIDADRList() ;
    this.getProductList();
    // tslint:disable-next-line:no-unused-expression
    this.photoLoaded = new BehaviorSubject<any>(null);
    this.photoUploaded = this.photoLoaded.asObservable();
    this.tempuser = JSON.parse(localStorage.getItem('logginguser'));
    if (!this.tempuser) {
      this.tempuser = new User();
      this.tempuser.userid = ' ';
    } else {
      this.getBuList(this.tempuser.oneview);
    }
    this.currentUser.next(this.tempuser);
  }
  getSrvList(imtext: string, location: string) {
    const lsrvList: SRVItem[] = [];
    let srvtokenstring = '';
    const srvtoken = JSON.parse(localStorage.getItem('srvtoken'));
    if (srvtoken) {
      srvtokenstring = ',srvtoken:' + srvtoken;
    } else {
      srvtokenstring = ' ';
    }
    const context = '{' + 'CONTRACTCODE:ABSA' +
      ',LOCATION:' + location + ',DICTIONARY:NOTIFS,TEXTIN:' + imtext + '}';
    const params = new HttpParams()
      .set('CallContext', context);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer 123456', 'apikey': 'GENAPP', 'runon': 'PROD'
    });
    this.http
      .get<any>(environment.BASE_API + '/api/sap/nlpt/nlptxt?Callcontext=' + context, { headers: headers })
      .subscribe(data => {
        if (data.ServicesList instanceof Array) {
          for (const rfqdoc of data.ServicesList) {
            const tobj = JSON.parse(rfqdoc.JsonsetJstext);
            if (tobj.NLEVEL === 3 && tobj.TYPE !== 'HIDEWEB') {
              const srvline: SRVItem = new SRVItem();
              srvline.nodekey = tobj.NODEKEY;
              srvline.description = tobj.FGDESCRIPTION.replace(/"/g, ' ');
              srvline.responsible = tobj.RESPONSIBLE;
              srvline.group = tobj.TECHID;
              srvline.type = tobj.TYPE;
              srvline.tag = tobj.NODECODE.replace(/"/g, ' ');
              srvline.inscope = tobj.ZACTIVE;
              lsrvList.push(srvline);
            }
          }
          this.currentSRVList.next(lsrvList);
        }
      });
  }
  getBuList(filter: string) {
   /* if ( filter === "") {
      filter = '\'\'';
    }*/
    const lbuList: BuLocation[] = [];
    const context = '{' + 'CONTRACTCODE:ABSA' +
      ',FILTER:' + filter + '}';
    const params = new HttpParams()
      .set('CallContext', context);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer 123456', 'apikey': 'GENAPP', 'runon': 'PROD'
    });
    this.http
      .get<any>(environment.BASE_API + '/api/sap/call/getbulist?Callcontext=' + context, { headers: headers })
      .subscribe(data => {
        if (data.ServicesList instanceof Array) {
          for (const budoc of data.ServicesList) {
            const tobj = JSON.parse(budoc.JsonsetJstext);
            const buline = <BuLocation>{};
            buline.building = tobj.XGETXT.replace(/"/g, ' ');
            buline.floc = tobj.TPLNR.replace(/"/g, ' ');
            buline.type = tobj.ZPRIDENTIFIER;
            buline.criticality = tobj.TKOMBUILDCLASS;
            lbuList.push(buline);
          }
          this.currentBUList.next(lbuList);
          if (lbuList.length === 1) {
            this.currentBU.next(this.currentBUList.value[0]);
          }
        }
      });
  }
  getUser(filter: string) {
    const context = '{' + 'CONTRACTCODE:ABSA' +
      ',PERSONFILTER:' + filter + '}';
    const params = new HttpParams()
      .set('CallContext', context);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer 123456', 'apikey': 'GENAPP', 'runon': 'PROD'
    });
    this.http
      .get<any>(environment.BASE_API + '/api/sap/call/getpartner?Callcontext=' + context, { headers: headers })
      .subscribe(data => {
        if (data.ServicesList instanceof Array) {
          for (const userdoc of data.ServicesList) {
            const tobj = JSON.parse(userdoc.JsonsetJstext);
            const userline = <User>{};
            userline.username = tobj.NAME1.replace(/"/g, ' ');
            userline.email = tobj.E_MAIL;
            userline.cellno = tobj.CELLPHONE;
            userline.oneview = tobj.ONEVIEW  || '';
            userline.costcentre = tobj.EXT_COSTCENTRE.replace(/"/g, ' ');
            userline.userid = tobj.PERCODE;
            userline.landline = tobj.TELEPHONE;
            userline.building = tobj.NAME1_UPPER;
            this.currentUser.next(userline);
            // Store
            localStorage.setItem('logginguser', JSON.stringify(userline));
          }

        }
      });
  }
  getBuFloorList(buildingid: string) {
    this.currentBUFloorList.next(['']);
    const lbuList = [];
    const context = '{' + 'CONTRACTCODE:ABSA' +
      ',BUILDING:' + buildingid + '}';
    const params = new HttpParams()
      .set('CallContext', context);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer 123456', 'apikey': 'GENAPP', 'runon': 'PROD'
    });
    this.http
      .get<any>(environment.BASE_API + '/api/sap/call/getbufloorlist?Callcontext=' + context, { headers: headers })
      .subscribe(data => {
        if (data.ServicesList instanceof Array) {
          for (const budoc of data.ServicesList) {
            const tobj = JSON.parse(budoc.JsonsetJstext);
            const buline = tobj.FLOOR.replace(/"/g, ' ');
            lbuList.push(buline);
          }
          this.currentBUFloorList.next(lbuList);
        }
      });
  }
  getqstList() {
    const lqList: any[] = [];
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer 123456', 'apikey': 'GENAPP', 'runon': 'PROD'
    });
    this.http
      .get<any>(environment.BASE_API + '/api/sap/call/getquestions?CallContext={CONTRACTCODE:"ABSA"} ', { headers: headers })
      .subscribe(data => {
        if (data.ServicesList instanceof Array) {
          for (const budoc of data.ServicesList) {
            const tobj = JSON.parse(budoc.JsonsetJstext);
            lqList.push(tobj);
          }
          this.currentQList.next(lqList);
        }
      });
  }
  getTitleList() {
    const ltList: any[] = [];
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer 123456', 'apikey': 'GENAPP', 'runon': 'PROD'
    });
    this.http
      .get<any>(environment.BASE_API + '/api/sap/call/gettitletexts?CallContext={CONTRACTCODE:"ABSA"} ', { headers: headers })
      .subscribe(data => {
        if (data.ServicesList instanceof Array) {
          for (const budoc of data.ServicesList) {
            const tobj = JSON.parse(budoc.JsonsetJstext);
            ltList.push(tobj);
          }
          if (ltList.length > 3) {
            this.currentTitleList.next(ltList);
          } else {
            this.currentTitleList.next(this.getTitles());
          }
        }
      });
  }
  getHelpList() {
    const lhList: any[] = [];
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer 123456', 'apikey': 'GENAPP', 'runon': 'PROD'
    });
    this.http
      .get<any>(environment.BASE_API + '/api/sap/call/gethelptexts?CallContext={CONTRACTCODE:"ABSA"} ', { headers: headers })
      .subscribe(data => {
        if (data.ServicesList instanceof Array) {
          for (const budoc of data.ServicesList) {
            const tobj = JSON.parse(budoc.JsonsetJstext);
            lhList.push(tobj);
          }
          if (lhList.length > 3) {
            this.currentHelpTextList.next(lhList);
          } else {
            this.currentHelpTextList.next(lhList);
          }
        }
      });
  }
  getProductList() {
    const lpList: any[] = [];
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer 123456', 'apikey': 'GENAPP', 'runon': 'PROD'
    });
    this.http
      .get<any>(environment.BASE_API + '/api/sap/call/getprodlist?CallContext={CONTRACTCODE:"ABSA"} ', { headers: headers })
      .subscribe(data => {
        if (data.ServicesList instanceof Array) {
          for (const budoc of data.ServicesList) {
            const tobj = JSON.parse(budoc.JsonsetJstext);
            const lclobj = { id: '', name: '', qty: '0', uom: '', group: ''};
            if ( tobj.TYPE === 'SERVICE') {
              const lsrvlist = [];
              const srvline: SRVItem = new SRVItem();
              srvline.nodekey = tobj.NODEKEY;
              srvline.description = tobj.FGDESCRIPTION.replace(/"/g, ' ');
              srvline.responsible = tobj.RESPONSIBLE;
              srvline.group = tobj.TECHID;
              srvline.tag = tobj.NODECODE.replace(/"/g, ' ');
              srvline.inscope = tobj.ZACTIVE;
              lsrvlist.push(srvline);
              const srvline2 = JSON.parse(JSON.stringify(srvline)) ;
              lsrvlist.push(srvline2);  // just to prevent auto return
           this.currentSRVList.next(lsrvlist);
              } else {
            lclobj[`id`] = tobj.NODEKEY;
            lclobj[`name`] = tobj.FGDESCRIPTION;
            lclobj[`qty`] = '0';
            lclobj.uom = tobj.TYPE ;
            lclobj.group = tobj.NODECODE ;
            if ( tobj.TYPE === 'CAREPACK') {
              this.lcplist.push(lclobj);
            } else {
            lpList.push(lclobj);
            }
          }
            this.currentProdList.next(lpList);
        }
      }
      });
  }
  setLocation(oneview: string) {
    this.getBuList(oneview);
    this.getBuFloorList(oneview);
  }
  createCall(newcall: CallModel): Observable<any> {
    this.calllogged.contractcode = 'ABSA';
    this.calllogged.key_level_1 = newcall.Location.building;
    this.calllogged.key_level_2 = newcall.Location.building;
    this.calllogged.ztplnr = newcall.Location.floc;
    this.calllogged.zfloor = newcall.Location.floor;
    this.calllogged.zroom = newcall.Location.locality;
    this.calllogged.znodekey = newcall.Service.nodekey;
    this.calllogged.longtext = newcall.Service.description;
    this.calllogged.zbp2nam = newcall.User.userid;
    this.calllogged.zemail = newcall.User.email;
    this.calllogged.zbpcell = newcall.User.cellno;
    this.calllogged.officeno = newcall.User.officeno;
    this.calllogged.costcentre = newcall.User.costcentre;
    this.calllogged.zpriority = newcall.RiskAnalysis.priority.substr(1, 1);
    this.calllogged.adrnr = newcall.Location.addrcode ;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
      })
    };
    const uploadvar = {
      callType: 'post',
      chContext: {
        CLASS: 'ABSA',
        METHOD: 'NEWNOTL'
      },
      chData: this.calllogged
    };
    return this.http
      .post<any>(environment.BASE_POST, uploadvar, httpOptions);
  }

  postPhotoFile(fileToUpload: File, notif: string): Observable<any> {
    const reader = new FileReader();
    reader.readAsDataURL(fileToUpload);
    reader.onload = (_event) => {
      this.fileContent = reader.result.toString().split(',').pop();
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Basic dXNlcm5hbWU6cGFzc3dvcmQ='
        })
      };
      const endpoint = environment.BASE_POST;
      const chdata = {
        'callType': 'post',
        'chContext': { 'CLASS': 'ATTACH', 'METHOD': '' },
        'chData': {
          'fileName': notif + '.jpg', 'fileSize': fileToUpload.size, 'fileType': 'jpg'
          , 'fileContent': this.fileContent
          , 'targetObjId': notif, 'targetObjType': 'BUS2080'
        }
      };
      this.http
        .post(endpoint, chdata, httpOptions).subscribe(data => {
          this.photoLoaded.next(data);
        });
    };
    return this.photoUploaded;
  }


  getTitles() {
    const itemin = { KEY: '', QUESTION: 'contact', LINE1: '', LINE2: '' };
    const itemList = [];
    itemin.KEY = 'contact';
    itemin.QUESTION = 'Contact Details';
    itemin.LINE1 = 'We need your contact details for feedback';
    itemin.LINE2 = 'Thanks';
    itemList.push(JSON.parse(JSON.stringify(itemin)));

    itemin.KEY = 'location';
    itemin.QUESTION = 'Location Details';
    itemin.LINE1 = 'Where is the problem located';
    itemin.LINE2 = 'Service Personnel need clear details Please';
    itemList.push(JSON.parse(JSON.stringify(itemin)));

    itemin.KEY = 'desccription';
    itemin.QUESTION = 'Description';
    itemin.LINE1 = 'Clearly describe the Problem';
    itemin.LINE2 = 'The clearer the description, the quicker service paersonnel can react';
    itemList.push(JSON.parse(JSON.stringify(itemin)));
    itemin.KEY = 'risk';
    itemin.QUESTION = 'What is the risk of the problem?';
    itemin.LINE1 = 'How risky is the problem to our brand or to a person\'s life';
    itemin.LINE2 = 'The higher the risk the quicker we can respond - at a cost though';
    itemList.push(JSON.parse(JSON.stringify(itemin)));
    itemin.KEY = 'summary';
    itemin.QUESTION = 'Summary of the Call thus far';
    itemin.LINE1 = 'You can change the call at any of the sections by clicking the button on the left of the line';
    itemin.LINE2 = 'You can add photo by uploading or via a mobile camera';
    itemList.push(JSON.parse(JSON.stringify(itemin))); itemList.push(itemin);
    return itemList;
  }
  getCOVIDADRList() {
    const ladrList: any[] = [];
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer 123456', 'apikey': 'GENAPP', 'runon': 'PROD'
    });
    this.http
      .get<any>(environment.BASE_API + '/api/sap/call/getcovidadr?CallContext={CONTRACTCODE:"COVID_ADR"} ', { headers: headers })
      .subscribe(data => {
        if (data.ServicesList instanceof Array) {
          for (const budoc of data.ServicesList) {
            const tobj = JSON.parse(budoc.JsonsetJstext);
            const lclobj = { Region: '', FullAddress: '', Attention: 'Unknown' , Telephone: 'Unknown'};
            lclobj[`addrcode`] = tobj.SEQ;
            lclobj[`Region`] = tobj.NODEKEY;
            lclobj[`FullAddress`] = tobj.VALUE;
            if (tobj.EXTRA) {
               const tarr = tobj.EXTRA.split(',');
               lclobj.Attention = tarr[0];
               if (tarr.length > 1) {
                 lclobj.Telephone = tarr[1];
               }
            }
            ladrList.push(lclobj);
            }
          }
            this.currentAdrList.next(ladrList);
        }) ;
      }


 getcovidadr() {
   return JSON.parse(`[
    {
      "Region": "Bloem",
      "FullAddress": "Bloem  Absa Provincial Head Office  c/o Nelson Mandela & Donald Murray Avenue  South Wing  3rd Floor  Bloemfontein ? 7942 attention Valerie Freyser "
    },
    {
      "Region": "Durban",
      "FullAddress": "Durban  Regional office  33 richefond circle  ridge side  Umhlanga ? 8217 attention Tracy Uys  "
    },
    {
      "Region": "East London",
      "FullAddress": "East London  Old Dyer & Dyer Building  49 Commercial Road  Arcadia  5209 ? 7001 attention Bongani Manziya  "
    },
    {
      "Region": "Epping",
      "FullAddress": "Epping  Unit 12 Omupark | Elliot Avenue | Epping 2 | Eppindust  7475 ? 7959 attention Claudine Muller    "
    },
    {
      "Region": "George",
      "FullAddress": "George  No. 2 Fabriek street  George; Industria  George  6529ÿ? 7952 attention Marcelle Claassen  "
    },
    {
      "Region": "Jhb",
      "FullAddress": "Jhb  4th Floor towers East  Johannesburg  170 main st JHB ? 8213 attention Lettie Mokoena   "
    },
    {
      "Region": "Kempton Park 6 ? Ziegler Building",
      "FullAddress": "Kempton Park 6 ? Ziegler Building  Ground Floor  136 Plane Road ? 7931 attention Jaco Abrie    "
    },
    {
      "Region": "Kimberly",
      "FullAddress": "Kimberly  80 Bultfonteinroad Absa building 3rd Floor KIMBERLEY 8301 ? 8777 attention Juliana De Waal     "
    },
    {
      "Region": "Klerksdorp",
      "FullAddress": "Klerksdorp 1 Chris Hani Road; Irene Park; Klerksdorp ? 2733 attention Alice vd Westhuizen     "
    },
    {
      "Region": "Mthatha",
      "FullAddress": "Mthatha  10 Errol Spring Street |Vulindlela Heights | MTHATHA ? 9146 attention Pelisa Mdipa     "
    },
    {
      "Region": "Nelspruit",
      "FullAddress": "Nelspruit  20 Paul Kruger St  Nelspruit ? 4793 Attention Judy Terblanche    "
    },
    {
      "Region": "Newcastle",
      "FullAddress": "Newcastle  4 Vlam Cresent Newcastle 2940 ? 2812 attention Annetjie Blom     "
    },
    {
      "Region": "Ormonde",
      "FullAddress": "Ormonde  Cnr Handel & Modulus Roads  Ormonde  Johannesburg South  2091 ? 7925 attention Thulile Semenya  "
    },
    {
      "Region": "PE",
      "FullAddress": "PE  2nd Floor  Moffet Office Park  cnr William Moffet & Overbaakens Str  Port Elizabeth 6000  South Africa ? 7950 attention Byron Smith"
    },
    {
      "Region": "Pietermaritzburg",
      "FullAddress": "Pietermaritzburg  Chatterton road branch  15 Chatterton Rd  PMB ? 6143 attention Khule Zondi   "
    },
    {
      "Region": "Polokwane",
      "FullAddress": "Polokwane  7 Veldspaat Street  Bendor  Polokwane  0699 ? 8361 attention Thandi Moshapo  "
    },
    {
      "Region": "Pretoria",
      "FullAddress": "Pretoria  C/o Battery and Petrol Street  Waltloo  Pretoria ? 2766 attention Rahab Matlala   "
    },
    {
      "Region": "Richards Bay ",
      "FullAddress": "Richards Bay   15 Betastraal Street  Alton  Richards Bay ? 6754 attention Simphiwe Cebekhule   "
    },
    {
      "Region": "Rustenburg",
      "FullAddress": "Rustenburg  Cnr Byers Naudeeÿ & Hefer Street; Rustenburg  SBV Building; 0299ÿ? 6121 attention Erna Heyneke    "
    },
    {
      "Region": "South Coast",
      "FullAddress": "South Coast  38 Berg Road  Marburg  Port Shepstone  4240 ? 6753 attention Thagen Naidoo  "
    },
    {
      "Region": "Vaal",
      "FullAddress": "Vaal  5 Fraser Street  Vanderbijlpark  1911ÿ - 4544 attention Riana Buitendag   "
    },
    {
      "Region": "Welkom",
      "FullAddress": "Welkom  222 Jan Hofmeyer road  Industria  Welkom  9459 ? 1241 attention Petro Olivier  "
    },
    {
      "Region": "Witbank",
      "FullAddress": "Witbank  10/11 Geodesic Road  Technopark  Witbank  1035 ? 0109 attention Jonas Mabena  "
    }
   ]`)
 }

}
