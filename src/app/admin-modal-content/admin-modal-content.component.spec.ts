import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminModalContentComponent } from './admin-modal-content.component';

describe('AdminModalContentComponent', () => {
  let component: AdminModalContentComponent;
  let fixture: ComponentFixture<AdminModalContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminModalContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminModalContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
