import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-admin-modal-content',
  templateUrl: './admin-modal-content.component.html',
  styleUrls: ['./admin-modal-content.component.css']
})
export class AdminModalContentComponent implements OnInit {

  @Input() public itemin: { KEYCODE: '', TITLE: '', QUESTION: '', LINE1: '', LINE2: '' };
  @Input() public action;
  constructor(public activeModal: NgbActiveModal) { }
  iteminx: { KEYCODE: '', TITLE: '', QUESTION: '', LINE1: '', LINE2: '' };
  ngOnInit() {
    this.iteminx = this.itemin;
  }
  dismiss() {
    this.activeModal.close();
  }
  saveItem() {
    this.activeModal.close();
  }

}
