import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QstnmodalContentComponent } from './qstnmodal-content.component';

describe('QstnmodalContentComponent', () => {
  let component: QstnmodalContentComponent;
  let fixture: ComponentFixture<QstnmodalContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QstnmodalContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QstnmodalContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
