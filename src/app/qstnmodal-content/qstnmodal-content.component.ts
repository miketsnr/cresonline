import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-qstnmodal-content',
  templateUrl: './qstnmodal-content.component.html',
  styleUrls: ['./qstnmodal-content.component.css']
})

export class QstnmodalContentComponent implements OnInit {
  @Input() public itemin;
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }
  dismiss() {
    this.activeModal.close();
  }
}
