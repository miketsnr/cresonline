import { Component, OnInit } from '@angular/core';
import { QstnmodalContentComponent } from '../qstnmodal-content/qstnmodal-content.component';
import { CallModel } from '../_models';
import { ApiServiceService } from '../utilities/api-service.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-calllog',
  templateUrl: './calllog.component.html',
  styleUrls: ['./calllog.component.css']
})
export class CalllogComponent implements OnInit {
  title = 'call-logger';
  public focusOn = 0;
  selectedLine = {};
  callcontainer = new CallModel();
  photoToUpload: File = null;
  myImageSrc: any;
  lastNotification: string;
  public titles: any[];

  constructor(public apiService: ApiServiceService, private modalService: NgbModal) {
    // this.apiService.getBuList('');
  }
  ngOnInit() {
    if (localStorage.getItem('calllogged')) {
  //    this.focusOn = 0;
    }
    this.apiService.photoUploaded.subscribe(data => {
      if (data && this.apiService.callcontainer.Service.description.length > 5) {
        this.focusOn = 6;

      }
    });
    this.apiService.tList.subscribe(data => {
      this.titles = data;
    })
    this.apiService.getTitleList();
  }
  /**
   * selectedBU
   * force change of Object for Angular update of dom
   * @param item
   */
  selectedBU(item) {
    this.focusOn = 3;
    this.callcontainer = JSON.parse(JSON.stringify(this.apiService.callcontainer));
  }
  /**
   * handleFileInput
   * Upload image to Server - see css and html to have custom look
   * @param files
   */
  handleFileInput(files: FileList) {
    this.photoToUpload = files.item(0);
    const reader = new FileReader();
    const preview = document.querySelector('#myImageSrc');
    reader.onloadend = function () {
      preview['src'] = reader.result;
    };

    if (this.photoToUpload) {
      reader.readAsDataURL(this.photoToUpload); //reads the data as a URL
    } else {
      this.myImageSrc = '';
    }
  }
  // To be called after Notif Created
  uploadPhotoToNotif(notif: string) {
    this.apiService.postPhotoFile(this.photoToUpload, notif).subscribe(data => {
      // do something, if upload success
      console.log(data);
    }, error => {
      console.log(error);
    });
  }
  useridentified(user) {
    this.focusOn++;

    this.callcontainer = JSON.parse(JSON.stringify(this.apiService.callcontainer));
  }

  serviceRequested(service: any) {
    if (this.apiService.callcontainer.RiskAnalysis.priority.length <= 1) {
      this.apiService.callcontainer.RiskAnalysis.priority = 'P5' ;
    }
    if ( service.group === 'COVID') {
      this.apiService.callcontainer.RiskAnalysis.priority = 'P5' ;
      this.focusOn++;
    }
    this.focusOn++;
    this.callcontainer = JSON.parse(JSON.stringify(this.apiService.callcontainer));
  }
  questionAnswered(priority: string) {
    this.focusOn++;
    this.callcontainer.RiskAnalysis.priority = priority.substring(0, 1);

    this.callcontainer = JSON.parse(JSON.stringify(this.apiService.callcontainer));
  }
  onSignup(_f) {

  }
  doReset() {
    window.location.assign(this.apiService.homeurl);
  }
  openModal(itemof) {
    let itemin = { QUESTION: 'contact', LINE1: '', LINE2: '' };
    this.titles.find(data => {
      if (data.KEYCODE == itemof) {
        itemin = data; return true;
      }
    });

    const modalRef = this.modalService.open(QstnmodalContentComponent);
    modalRef.componentInstance.itemin = itemin;
  }
  onSubmit(call: CallModel) {
    this.apiService.createCall(call).subscribe(
      data => {
        console.log(data); // now load photo
        const objin = JSON.parse(JSON.parse(data.d.chObjid));
        let  s1 = objin.QMNUM ;
        this.lastNotification = s1.replace(/^0+/, '');
        this.focusOn = 6;
        if (this.photoToUpload) {
          this.uploadPhotoToNotif(s1);
        }
      },
      e => {
        console.log(e);
      }
    );
  }
  goforit() {
    this.focusOn = 1;
    this.apiService.currentFocus.next('1');
    localStorage.setItem('calllogged', 'X');
  }
  greaterThan(subj: any, num: any) {
    return subj > num;
  }
  gTorEq(subj: any, num: any) {
    return subj > num || subj === num;
  }
  lessThan(subj: any, num: any) {
    return subj < num;
  }
  lTorEq(subj: any, num: any) {
    return subj < num || subj === num;
  }
  change(what: string) {
    switch (what) {
      case 'username': {
        this.focusOn = 1;
        break;
      }
      case 'location': {
        this.focusOn = 2;
        break;
      }
      case 'service': {
        this.focusOn = 3;
     //   this.apiService.currentSRVList.next(null);
        break;
      }
      case 'risk': {
        this.focusOn = 4;
        break;
      }
    }
  }

}
