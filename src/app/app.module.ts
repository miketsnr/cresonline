import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ModalModule } from './_modal';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalllogComponent } from './calllog/calllog.component';
import { IdentifyUserComponent } from './identify-user/identifyUser.component';
import { SearchpipePipe } from './utilities/searchpipe.pipe';
import { SearchFormComponent } from './location-form/search-form.component';
import { ProblemIdComponent } from './problem-id/problem-id.component';
import { HttpClientModule } from '@angular/common/http';
import { QuestionsComponent } from './questions/questions.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QstnmodalContentComponent } from './qstnmodal-content/qstnmodal-content.component';
import { AdminComponent } from './admin/admin.component';
import { AdminModalContentComponent } from './admin-modal-content/admin-modal-content.component';

@NgModule({
  declarations: [
    AppComponent,
    CalllogComponent,
    IdentifyUserComponent,
    SearchpipePipe,
    SearchFormComponent,
    ProblemIdComponent,
    QuestionsComponent,
    QstnmodalContentComponent,
    AdminComponent,
    AdminModalContentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ModalModule,
    ReactiveFormsModule,
    HttpClientModule, NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [QstnmodalContentComponent, AdminModalContentComponent]
})
export class AppModule { }
