import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ApiServiceService } from '../utilities/api-service.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { QstnmodalContentComponent } from '../qstnmodal-content/qstnmodal-content.component';
import { from } from 'rxjs';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {
  @Output() selectedquestion = new EventEmitter();
  qlist = [];
  questionlist = [];
  questionlist2 = [];
  selectednodes = [];
  results = '';
  resultstxt = '';
  level2 = 'X';
  constructor(private apiService: ApiServiceService, private modalService: NgbModal) {

  }

  ngOnInit() {
    this.apiService.getqstList();
    this.apiService.currentQList.subscribe(
      response => {
        this.qlist = response;
        if (Array.isArray(response)) {
          response.forEach(itemin => {
            if (!itemin.LINE1) {
              itemin.LINE1 = 'A Better explanation';
              itemin.LINE2 = 'And even more detail';
            }
          });
        }
        if (response) {
          this.buildQuiz();
        }
      }

    );
  }
  buildQuiz() {
    /*    const item = this.qlist.find(itemin => {
          if (itemin.PARENT === 'ROOT') {
            return true;
          }
        });*/
    this.questionlist = [];
    this.questionlist2 = [];
    this.qlist.forEach(item => {
      if (item.PARENT === 'ROOT') {
        this.questionlist.push(item);
      } else {
        this.questionlist2.push(item);
      }

    });

  }
  level1clicked(item, response) {

    if (response !== 'N') {
      this.results =  item.AFFIRM;
      this.resultstxt =  item.AFFIRM;
      this.level2 = 'X';
    } else {
      this.results = '';
      this.resultstxt = '';
      this.level2 = '';
    }
  }
  level2clicked(item, response) {
    this.results = item.AFFIRM;
    this.resultstxt = item.AFFIRM;
  }
  nodeclicked(item, response) {
    let keyin = '';
    this.results = '';
    if (response !== 'N') {
      keyin = item.AFFIRM + '-' + item.NODEKEY;
    } else {
      keyin = item.DECLINE + '-' + item.NODEKEY;
    }
    if (keyin.slice(0, 1) === 'P') {
      this.results = keyin.split('-')[0];
      switch (this.results) {
        case 'P1': {
          this.resultstxt = this.results + '- Emergency';
          break;
        }
        case 'P2': {
          this.resultstxt = this.results + '- Urgent';
          break;
        }
        case 'P3': {
          this.resultstxt = this.results + '- Important';
          break;
        }
        case 'P4': {
          this.resultstxt = this.results + '- Standard Time';
          break;
        }
      }

    } else {
      this.results = '';
      this.resultstxt = '';
    }

    for (let i = 0; i < this.selectednodes.length; i++) {

      if (this.selectednodes[i].split('-')[1] === keyin.split('-')[1]) {
        this.selectednodes.length = i;
      }
    }
    this.buildQuiz();
    this.selectednodes.push(keyin);
    this.selectednodes.forEach(node => {
      const newitem = this.qlist.find(itemin => {
        return itemin.NODEKEY === node.split('-')[0];
      });
      if (newitem) {
        this.questionlist.push(newitem);
      }
    });

  }
  isselectedNode(node1: string, node2: string) {
    const keyin = node1 + '-' + node2;
    const outresult = this.selectednodes.find(node => {
      return (node === keyin);
    }
    );
    if (outresult) { return true; } else { return false; }
  }
  onQstDone() {
    this.apiService.callcontainer.RiskAnalysis.priority = this.resultstxt;
    this.selectedquestion.emit(this.resultstxt);
  }
  openModal(itemin) {
    const modalRef = this.modalService.open(QstnmodalContentComponent);
    modalRef.componentInstance.itemin = itemin;
  }
}
