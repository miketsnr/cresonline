﻿export class User {
  username: string;
  firstName: string;
  lastName: string;
  role: string;
  cellno: string;
  sundry: string;
  commmethod: string;
  officeno: string;
  email: string;
  costcentre: string;
  userid: string;
  oneview: string;
  building: string;
  landline: string;
}


