import { User } from './user';
export class BuLocation {
  building: string;
  floc: string;
  type: string;
  criticality: string;
  floor: string;
  locality: string;
  oneview: string;
  addrcode: string;
}

export class SRVItem {
  id: number;
  group: string;
  nodekey: string;
  responsible: string;
  description: string;
  tag: string;
  inscope: string;
  type: string;
}

export class CallModel {
  constructor() {
    this.User = new User();
    this.Service = new SRVItem();
    this.Location = <BuLocation>{};
    this.RiskAnalysis = { priority: '5' };
  }
  User: User;
  Location: BuLocation;
  Service: SRVItem;
  RiskAnalysis: {
    priority: string
  };

}

export class CallLog {
  contractcode = '';
  longtext = '';
  key_level_1 = '';
  key_level_2 = '';
  zfloor = '';
  zroom = '';
  ztplnr = '';
  znodekey = '';
  zpriority = '';
  zbp2nam = '';
  zemail = '';
  officeno = '';
  zbpcell = '';
  costcentre = '';
  adrnr = '';
}
