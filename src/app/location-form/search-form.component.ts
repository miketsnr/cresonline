import { Component, Input, Output, OnInit } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { ApiServiceService } from '../utilities/api-service.service';
import { BuLocation } from '../_models';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {
  @Input() searchItems: any[];
  @Output() selectedItem = new EventEmitter();
  public searchlist = [];
  public searchbu = '';
  public bulocation = {building: '', floc: '', floor: '', locality: ''};
  public searchbox: any;
  public allFloors = [''];


  constructor(public apiService: ApiServiceService) {
    this.apiService.currentLocation.subscribe(datain => {
      /*
      this.bulocation.building = this.apiService.callcontainer.Location.building;
      this.bulocation.floc = this.apiService.callcontainer.Location.floc;
      this.bulocation.floor = this.apiService.callcontainer.Location.floor;
      this.bulocation.locality = this.apiService.callcontainer.Location.locality;
      */
     if (datain == null) {
      this.bulocation.building =  '';
      this.bulocation.floc =  '';
      this.bulocation.floor =  '';
      this.bulocation.locality =  '';
     } else {
      this.bulocation.building = datain.building || '';
      this.bulocation.floc = datain.floc  || '';
      this.bulocation.floor = datain.floor  || '';
      this.bulocation.locality = datain.locality  || '';
     }

    });
  }
  ngOnInit() {

    this.apiService.currentLocation.subscribe(data => {
      if (data != null && data.building != null) {
      this.bulocation = data;
       } else {
         this.bulocation = {building: '', floc: '', floor: '', locality: ''};
      }
    });
    this.apiService.buList.subscribe((item) => {
      if ( item && item.length > 0 ) {
      this.searchlist = [];
      item.forEach( itemin => {
        this.searchlist.push(JSON.parse(JSON.stringify(itemin)));
      } ); }
    } ,
      (err: any) => {

      },
      () => {

      } ,
    );
    this.apiService.floorlist.subscribe((item: []) => {
      this.allFloors = item;
      if (this.allFloors && this.allFloors.length === 1) {
        this.bulocation.floor = this.allFloors[0];
      }
    },
      (err: any) => {

      },
      () => {

      } ,
    );
    if (this.bulocation.building === '') {
      this.getbulist() ;
    }
  }
  /**
   *
   */
  getbulist() {
    this.searchbu = 'X';
    this.apiService.getBuList('');
  }

  buchanged(item) {
    this.searchbu = '';
    this.bulocation.building = item.building;
    this.bulocation.floc = item.floc;
    this.apiService.getBuFloorList(this.bulocation.floc);

  }
  /*
*/
  floorChanged(floor: string) {
    this.allFloors = ['First Floor'];
    this.bulocation.floor = floor;
  }
  /*
  */
  acceptitem(item: any) {
    this.apiService.callcontainer.Location.building = this.bulocation.building;
    this.apiService.callcontainer.Location.floc = this.bulocation.floc;
    this.apiService.callcontainer.Location.floor = this.bulocation.floor;
    this.apiService.callcontainer.Location.locality = this.bulocation.locality;
    this.selectedItem.emit(this.bulocation);
  }
  /*
*/
  lessthan(a, b) {
    return a < b;
  }

}
