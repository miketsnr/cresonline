import { Component, Output } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidationErrors } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import { ApiServiceService } from '../utilities/api-service.service';
import { User } from '../_models';

@Component({
  selector: 'app-identifyuser',
  templateUrl: './identifyUser.component.html',
  styleUrls: ['./identifyUser.component.css']
})
export class IdentifyUserComponent {
  @Output() enteredForm = new EventEmitter();
  public user = new User();
  public check = '';
  constructor(public apiService: ApiServiceService) {
    this.apiService.loggingUser.subscribe(data => {
      this.user = data;
      if (this.user.oneview.length < 4) {
        this.user.building = '';
      }
      this.check = data.userid;
    });
  }

  findUser(filter) {
    this.apiService.getUser(filter);

  }

  onSubmit(f) {
    if (this.check !== this.user.userid || this.user.userid === 'INVALID') {
      return;
    }
    this.apiService.callcontainer.User.username = this.user.username;
    this.apiService.callcontainer.User.cellno = this.user.cellno;
    this.apiService.callcontainer.User.email = this.user.email;
    this.apiService.callcontainer.User.landline = this.user.landline;
    this.apiService.callcontainer.User.costcentre = this.user.costcentre;
    this.apiService.callcontainer.User.userid = this.user.userid;
    if (this.user.oneview.length < 4) {this.user.oneview = ''; }
    if (this.apiService.callcontainer.Location.oneview !== this.user.oneview) {
      this.apiService.setLocation(this.user.oneview);

    }
    this.enteredForm.emit(f);
  }

}
